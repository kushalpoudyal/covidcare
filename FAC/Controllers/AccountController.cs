﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FAC.Entity.Identity;
using FAC.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MySqlX.XDevAPI;

namespace FAC.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        public AccountController(UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> RegisterServices(RegistrationViewmodel model)
        {
            var UserType = string.Empty;
            if (ModelState.IsValid)
            {
                if (model.IAgree)
                {

                    var user = new ApplicationUser() { Id = Guid.NewGuid().ToString(), UserName = model.Username, Email = model.Email };
                    var result = await userManager.CreateAsync(user, model.Password);

                    if (result.Succeeded)
                    {
                        user.Fullname = model.Fullname;
                        user.Location = model.Location;

                        await userManager.UpdateAsync(user);

                        try
                        {
                            await this.userManager.AddToRoleAsync(user, "Doctor");//Doctor,Lab,Ambulance

                        }
                        catch
                        {

                        }

                        return RedirectToAction("Home", "Index");

                    }
                    foreach (var item in result.Errors)
                    {
                        ModelState.AddModelError("", item.Description);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Do You Agree with all terms and condition ?");
                }

            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> RegisterUser(string Username, string Password)
        {
            var UserType = string.Empty;
            if (ModelState.IsValid)
            {


                var user = new ApplicationUser() { Id = Guid.NewGuid().ToString(), UserName = Username };
                var result = await userManager.CreateAsync(user, Password);

                if (result.Succeeded)
                {
                    user.IsPasswordUpdated = false;

                    await userManager.UpdateAsync(user);

                    try
                    {
                        await this.userManager.AddToRoleAsync(user, "Patient");//Doctor,Lab,Ambulance

                    }
                    catch
                    {

                    }

                    return RedirectToAction("Home", "Index");

                }
                foreach (var item in result.Errors)
                {
                    ModelState.AddModelError("", item.Description);
                }
            }
            else
            {
                ModelState.AddModelError("", "Do You Agree with all terms and condition ?");
            }
            return View();

        }

        public async Task<bool> LoginCheck(string Username)
        {
            var user = await userManager.FindByNameAsync(Username);
            if (user == null)
            {
                //send OTP
                HttpContext.Session.SetString("OTP", "123456");
                return true;
            }
            else
            {
                return false;
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(string Username, string Password)
        {
            if (ModelState.IsValid)
            {

                var result = await signInManager.PasswordSignInAsync(Username, Password, true, false);
                if (result.Succeeded)
                {
                    var usrdetails = await userManager.GetUserAsync(User);
                    var usrRole = await this.userManager.GetRolesAsync(usrdetails);

                    if (HttpContext.User != null)
                    {
                        var claims = new List<Claim>
                     {
                               new Claim(ClaimTypes.Role,usrRole.FirstOrDefault()),
                               new Claim(ClaimTypes.NameIdentifier,usrdetails.Id.ToString()),

                    };

                        var appIdentity = new ClaimsIdentity(claims);
                        HttpContext.User.AddIdentity(appIdentity);
                    }


                }

                ModelState.AddModelError("", "Invalid Login Attempt");

            }
            return View();
        }
    }
}