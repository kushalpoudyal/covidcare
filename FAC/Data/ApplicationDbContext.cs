﻿using System;
using System.Collections.Generic;
using System.Text;
using FAC.Entity.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FAC.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>().ToTable("ApplicationUser");
            modelBuilder.Entity<ApplicationRole>().ToTable("ApplicationRole");

            modelBuilder.Entity<ApplicationUser>().Property(h => h.Id).HasMaxLength(36).IsRequired();
            modelBuilder.Entity<ApplicationRole>().Property(h => h.Id).HasMaxLength(36).IsRequired();



        }
    }
}
