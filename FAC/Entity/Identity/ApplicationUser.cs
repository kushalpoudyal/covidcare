﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FAC.Entity.Identity
{
    public class ApplicationUser : IdentityUser<string>
    {
        public string Fullname { get; set; }

        public string Location { get; set; }

        public bool IsPasswordUpdated { get; set; }
    }
}
